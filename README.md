![Logo](resources/banner.png)

# 🎮 Tesseract Game Engine

A fun (and experimental) game engine written in Java

## License

This engine is licensed under the [BSD 3-Clause “New” or “Revised” License](https://choosealicense.com/licenses/bsd-3-clause/). You can see the fulltext license [here](LICENSE).
